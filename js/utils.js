var utils = function () {
  "use strict";
  return {
    wasInside: true,
    init: function () {
      utils.configModal();
      this.initValueScroll();
      jQuery(window).on('scroll', utils.scrollHeader);
      jQuery("#searchTerm").focus(utils.searchFocus);
      jQuery("#searchTerm").focusout(utils.searchOutFocus);
      utils.tooltip('button');     
    },

    // Función que agrega contador de carácteres a textarea
    countCharacter: function (idTextArea, maxCharacters) {
      var text = document.getElementById(idTextArea);
      var wrapper = document.createElement('div');
      var c_wrap = document.createElement('div');
      var count = document.createElement('span');
      var message = document.createElement('span');

      wrapper.className = "div-character";
      wrapper.id = "div" + idTextArea;
      c_wrap.innerHTML = '';
      c_wrap.className = "div-count-character";

      // Mensaje de advertencia
      message.className = "hidden span-message-character";
      message.id = "message" + idTextArea;
      message.innerText = "Alcanzó el máximo de carácteres permitidos";

      // Contador de carácteres
      count.className = "float-right pr-3 span-count-character";
      count.id = "count" + idTextArea;

      text.parentNode.appendChild(wrapper);
      wrapper.appendChild(text);

      c_wrap.appendChild(message);
      c_wrap.appendChild(count);
      wrapper.appendChild(c_wrap);

      function _set() {
        count.innerHTML = (maxCharacters - this.value.length) || 0;
        var countCharacters = maxCharacters - this.value.length;
        var elementMessage = jQuery("#div" + idTextArea + " #message" + idTextArea);
        var elementCount = jQuery("#div" + idTextArea + " #count" + idTextArea);
        if (countCharacters <= 0) {
          elementMessage.removeClass("hidden");
          elementCount.css("color", "red");
        } else if (elementMessage.hasClass("hidden") == false) {
          elementMessage.addClass("hidden")
          elementCount.css("color", "#0B457F");
        }
        count.innerHTML = countCharacters || 0;
      }
      text.addEventListener('input', _set);
      _set.call(text);
    },

    // Función que valida si sale del sitio
    leaveSite: function (url) {
      let message = "Con esta acción abrirás una nueva pestaña. ¡Te esperamos pronto!";
      let opcAction = [
        {
          text: "Cancelar",
          action: "utils.hideModal('alert-modal')",
          class: "btn-middle"
        },
        {
          text: "Continuar",
          action: "utils.modalGoTo('" + url + "', 'alert-modal')",
          class: "btn-high"
        }
      ];
      utils.callModalAlert("warning", "Estás saliendo de GOV.CO", message, opcAction);
    },

    examplesAlerts: function(tipo) {
      let opcAction = [
        {
          text: "Cancelar",
          action: "utils.hideModal('alert-modal')",
          class: "btn-middle"
        }
      ];
      utils.callModalAlert(tipo, 'Modal tipo '+tipo, 'Información de detalle al cierre de la acción', opcAction)
    },
    
    // Función obtiene modal de mantenimiento
    maintenanceSite: function () {
      let message = "Esta página esta en mantenimiento en breve estaremos de vuelta.<br><br>";
      message += "Puedes escribirnos a soporteccc@mintic.gov.co.<br>Llámanos gratis: 01 8000 910742 y en Bogotá: 3 90 79 51";
      let opcAction = [];
      utils.callModalAlert("maintenance", "En mantenimiento", message, opcAction);
    },

    // Función obtiene modal de error de sitio
    errorSite: function () {
      let  message = "Puedes escribirnos a soporteccc@mintic.gov.co.<br>Llámanos gratis: 01 8000 910742 y en Bogotá: 3 90 79 51";
      let opcAction = [
        {
          text: "Regresar",
          action: "utils.hideModal('alert-modal')",
          class: "btn-high"
        }
      ];
      utils.callModalAlert("error-site", "Ha ocurrido un error", message, opcAction);
    },

    // Función que abre otra ventana del navegador con Url de parametro
    goTo: function (url) {
      if (url.length > 0 &&
        url != "undefined") {
        window.open(url, '_blank');
      }
    },

    // Función que ajusta posición de modal sobre main y backdrop
    configModal: function(){

      // Copy modals outher main
      jQuery("#modals-content").append(jQuery(".modal"));
      jQuery('.modal').on('shown.bs.modal', function() {
        //Make sure the modal and backdrop are siblings (changes the DOM)
        jQuery(this).before(jQuery('.modal-backdrop'));
        //Make sure the z-index is higher than the backdrop
        jQuery(this).css("z-index", parseInt(jQuery('.modal-backdrop').css('z-index')) + 1);
      });
    },

    // Función que ajusta posición de un modal especifico sobre main y backdrop
    configBackDrop: function(idModal){
      jQuery('#'+idModal).on('shown.bs.modal', function() {
        //Make sure the modal and backdrop are siblings (changes the DOM)
        jQuery(this).before(jQuery('.modal-backdrop'));
        //Make sure the z-index is higher than the backdrop
        jQuery(this).css("z-index", parseInt(jQuery('.modal-backdrop').css('z-index')) + 1);
      });
    },

    // Función que invoca un modal de tipo alerta
    callModalAlert: function (typeModal, title, message, opc) {
      var iconModal = "";
      if(jQuery(".alert-govco-maintenance").hasClass("hidden") == false) {
        jQuery(".alert-govco-maintenance").addClass("hidden") 
      }
      jQuery(".alert-modal .modal-header i").show();
      jQuery("#modal-content-type-alert .alert-icon #govco-icon-alert").removeClass();
      switch (typeModal) {
        case "error":
          iconModal = "govco-icon govco-icon-x-cn";
          jQuery(".alert-modal .modal-header i").hide();
          break;
        case "warning":
          iconModal = "govco-icon govco-icon-exclamation-cn";
          jQuery(".alert-modal .modal-header i").hide();
          break;
        case "success":
          iconModal = "govco-icon govco-icon-circle-check-n";
          jQuery(".alert-modal .modal-header i").hide();
          break;
        case "info":
          iconModal = "govco-icon govco-icon-circle-check-n";
          break;
        case "maintenance":
          iconModal = "govco-icon govco-icon-exclamation-cn";
          jQuery(".alert-govco-maintenance").removeClass("hidden");
          break;
        case "error-site":
          iconModal = "govco-icon govco-icon-sad-face-n";
          jQuery(".alert-modal .modal-header i").hide();
          break;
        default:
          iconModal = "govco-icon govco-icon-exclamation-cn";
      }

      jQuery("#modal-content-type-alert .alert-icon #govco-icon-alert").addClass(iconModal);
      jQuery("#modal-content-type-alert").removeClass();
      jQuery("#modal-content-title").removeClass();
      jQuery("#modal-content-title").addClass('modal-content-title content-govco');
      jQuery("#modal-content-type-alert").addClass("modal-content-" + typeModal);
      jQuery("#modal-content-title").addClass("modal-content-"+typeModal);     
      jQuery(".alert-modal .modal-footer").html(utils.createButtonsModal(opc));
      jQuery("#modal-content-title").text(title);
      jQuery("#modal-content-txt").html(message);
      jQuery(".alert-modal").modal({
        backdrop: 'static',
        keyboard: false
      });
    },

    // Call Tooltip function
    tooltip: function(selector) {
      $(selector).tooltip();
    },

    // Función que oculta modal
    hideModal: function (idModal) {
      jQuery("." + idModal).modal("hide");
    },

    // Función que captura eventos de modal alert
    eventsModalAlert: function () {
      $('.alert-modal').on('hide.bs.modal', function () {
        jQuery("#modal-content-txt").text(message);
        jQuery(".alert-modal .modal-footer").html("");
      })
    },

    // Función que crea HTML de listado de botones
    createButtonsModal: function (listOpc) {
      var footer = "";
      listOpc.forEach(function (item) {
        footer += '<button type="button" class="btn btn-round btn-modal ' + item.class + '"';
        footer += 'onclick="' + item.action + '">' + item.text + '</button>';
      });
      return footer;
    },

    // Función que abre otra ventana del navegador con Url de parametro
    modalGoTo: function (url, idModal) {
      utils.hideModal(idModal);
      utils.goTo(url);

    },

    // Evento de clic sobre componente collapse
    collapseOnClick: function (classComponent, idLoop) {
      let state = jQuery("#item-" + idLoop).attr('aria-expanded');
      jQuery("." + classComponent + " .lbl-minus").hide();
      jQuery("." + classComponent + " .icon-minus").hide();
      jQuery("." + classComponent + " #title-entity-" + idLoop).show();
      jQuery(".content-" + classComponent + " .icon-add").show(100);
      jQuery("#title-" + idLoop).show(200);
      if (state == 'false') {
        jQuery("#item-" + idLoop + " .lbl-minus").show(200);
        jQuery("#item-" + idLoop + " .icon-minus").show(200);
        jQuery("#item-" + idLoop + " #icon-add-" + idLoop).hide(200);
        jQuery("." + classComponent + " .title-entity-" + classComponent + ":not(#title-entity-" + idLoop + ")").hide(200, function () {
        });
        jQuery("." + classComponent + " .collapse-title:not(#item-" + idLoop + ")").addClass("pb-2");
      } else {
        jQuery("#item-" + idLoop + " .lbl-minus").hide();
        jQuery("#item-" + idLoop + " .icon-minus").hide();
        jQuery(".title-entity-" + classComponent).show(200);
        jQuery("." + classComponent + " .title-entity-" + classComponent).show(200, function () {
        });
        jQuery("." + classComponent + " .collapse-title").removeClass("pb-2");
      }
    },

    callModal: function(type){
      jQuery(".govco-modal-"+type).modal({
        backdrop: 'static',
        keyboard: false
      });
    },
     // Función de evento para activar evento scroll y sus interacciones
     scrollHeader: function(){
      const scrollOffset = jQuery(document).scrollTop();
     
      const firstMenu = document.getElementsByClassName('nav-primary')[0];
      const secondMenu = document.getElementsByClassName('nav-secondary')[0];
      const itemFirstMenu = document.getElementsByClassName('nav-item-primary')[0];
      const searchNavbar = document.getElementsByClassName('search-navbar')[0];
      if (firstMenu != undefined && secondMenu != undefined){
        // Valida la posición del scroll para activar/inactivar animación
        if (scrollOffset < 300) { utils.initValueScroll(); } else {
          firstMenu.classList.remove('hidden-transition');
          secondMenu.classList.remove('show-transition');
          if (firstMenu.classList.contains('show-transition') === false) {
            firstMenu.classList.add('show-transition');
            itemFirstMenu.classList.remove('is-scroll');
          }
          if (secondMenu.classList.contains('hidden-transition') === false) {
            secondMenu.classList.add('hidden-transition');
          }
          if (searchNavbar.classList.contains('translation') === false) {
            searchNavbar.classList.add('translation');
            searchNavbar.classList.remove('non-translation');
          }
        }
      }
    },
    // Función para asignar valores iniciales a la animación de segundo menú
    initValueScroll() {
      const firstMenu = document.getElementsByClassName('nav-primary')[0];
      const secondMenu = document.getElementsByClassName('nav-secondary')[0];
      const itemFirstMenu = document.getElementsByClassName('nav-item-primary')[0];
      const searchNavbar = document.getElementsByClassName('search-navbar')[0];
      if (firstMenu != undefined && secondMenu != undefined){
        if (firstMenu.classList.contains('hidden-transition') === false) {
          itemFirstMenu.classList.add('is-scroll');
          firstMenu.classList.add('hidden-transition');
          firstMenu.classList.remove('show-transition');
        }
        if (secondMenu.classList.contains('show-transition') === false) {
          secondMenu.classList.remove('hidden-transition');
          secondMenu.classList.add('show-transition');
        }
        if (searchNavbar.classList.contains('translation')) {
          searchNavbar.classList.remove('translation');
          searchNavbar.classList.add('non-translation');
        }
      }
    },
    // Header Nvl 2: Función para activar la animación de búsqueda
    searchFocus() {
      const searchBar = document.getElementsByClassName('form-search-bar')[0];
      searchBar.classList.add('form-search-bar-active');
    },
    // Header Nvl 2: Función para ocultar la animación de búsqueda
    searchOutFocus() {
      const searchBar = document.getElementsByClassName('form-search-bar')[0];
      searchBar.classList.remove('form-search-bar-active');
    },
    onInputPassword(){
      const kb = document.getElementById('keyboard');
      kb.classList.add('show');
      this.wasInside = true;
    },
    outInputPassword(){
      const kb = document.getElementById('keyboard');
      if (this.wasInside) {
        kb.classList.remove('show');        
      }  
      this.wasInside = false;    
    }
  }
}();

jQuery(function () {
  utils.init();

  ///Evento para el input del buscador del header
  jQuery("#input-buscador-header, #input-buscador-header-mobile").on("keyup", function (e) {
    if (e.which == 13) {
      var filtro = jQuery(this).val();
      RedirectBuscadorConFiltro(filtro);
    }
  });

  jQuery("#input-buscador-header_btn-search").on("click", function (e) {
    var filtro = jQuery("#input-buscador-header").val();
    RedirectBuscadorConFiltro(filtro);
  });

  //Funcionalidad back to top
  const backToTopButton = document.querySelector(".btn-up-hover-clone");
  if (backToTopButton != null){
    window.addEventListener("scroll", function(){
      if(window.pageYOffset > 300){
        backToTopButton.classList.add('show'); //fade in
        backToTopButton.style.display = "block";
        backToTopButton.classList.remove('hide');
      }else{
        backToTopButton.classList.add('hide'); //fade out
        backToTopButton.classList.remove('show');
      }
      backToTopButton.addEventListener("click", function(){
        window.scrollTo(0,0);
      }); 
    });
  }

  //Funcionalidad barra de accesibilidad
  var letterMin = document.querySelector('.min-fontsize');
  var letterMax = document.querySelector('.max-fontsize');
  var mood=1;
  var size = parseInt(getComputedStyle(document.documentElement).fontSize);

  //Contraste
  document.querySelector('.contrast-ref').addEventListener('click', function(){
    if(mood==1){
      document.body.classList.add('all');
      mood++;
    }else{
      document.body.classList.remove('all');
      mood--;
    }
  });

  //reducir letra
  letterMin.addEventListener('click', function(){
    //var size = parseInt(getComputedStyle(document.documentElement).fontSize);
    if (size > 10) {
      size--;
      font = size.toString();
      document.querySelector('html').style.fontSize = font +'px';
    }
  });
 
  //Aumentar Letra
  letterMax.addEventListener('click', function(){
    //var size = parseInt(getComputedStyle(document.documentElement).fontSize);
    if (size > 10) {
      size++;
      font = size.toString();
      document.querySelector('html').style.fontSize = font +'px';
    }
  });

});

function RedirectBuscadorConFiltro(filtro){
  if (filtro)
    window.location.href = "/buscador?busqueda=" + filtro;
}
