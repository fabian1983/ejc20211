w3IncludeHTML();  

url_pagina = 
[
    "https://ejercito.mil.co/", 
    "https://primeradivision.mil.co/"
];

array_home_principal = 
[
    [
        [1, 2, 1, 0],
        [2, 20, 1, 79],
        [3, 35, 1, 0],
        [4, 3, 1, 0],
        [5, 25, 1, 0],
        [6, 0, 1, 0],
        [7, 87, 1, 0]
    ], 
    [
        [1, 57, 1, 0],
        [2, 20, 1, 79],
        [3, 35, 1, 0],
        [4, 3, 1, 0 ],
        [5, 25, 1, 0],
        [6, 55, 1, 0],
        [7, 87, 1, 0]
    ]
];


url_pagina_prueba                =      "https://local.ejercitomilco.co/";
url_principal                    =      "https://dicoe.mil.co/";
url_api_home_header_footer       =      "https://dicoe.mil.co/recurso_user/json_programa/800130633.json";
url_api                          =      "https://dicoe.mil.co/classes_php/class_parametrizacion_web.php";
nit                              =      "800130633";
digito_verificacion              =      "4";
latitud_actual                   =      "";
longitud_actual                  =      "";
array_home                       =      "";
playing_stream                   =      true;
elemento                         =      $('html, .card-title');
sizeFuenteOriginal               =      elemento.css('font-size');
contrate                         =      0;

$(document).ready(function()
{
    $('.venobox').venobox
    ({
        border     : '0px',                             // default: '0'
        bgcolor    : '#fff',                          // default: '#fff'
        titleattr  : 'data-title',                       // default: 'title'
        numeratio  : true,                               // default: false
        infinigall : true,                               // default: false
        share      : ['facebook', 'twitter', 'download'] // default: []
    });      
});

window.onscroll = function()
{
    var scrollTop = window.scrollY;

    if (scrollTop > 200) 
    {
        $(".scroll-to-top").addClass("show-scrollTop")    
    } 
    else 
    {
        $(".scroll-to-top").removeClass("show-scrollTop")
    }
};

function class_paginacion(numero_pagina, boton_pagina)
{
    $(".page-link").css("color", "");

    if (numero_pagina != "" && boton_pagina != "")
    {
        paginas = $(".paginacion").toArray().length;
        $(".paginacion").addClass("hidden");
        $("#"+numero_pagina).removeClass("hidden");
        $(".boton_paginacion").removeClass("active");
        $("#boton_paginacion_"+boton_pagina).addClass("active");
        $(".color_"+boton_pagina).css("color", "#fff")

        $("html, body").animate({
            scrollTop: 290
        }, 2000);
    }
    else
    {
        $(".color_1").css("color", "#fff")
        $("#boton_paginacion_1").addClass("active");
    }
}

function class_nav_cerrar_menu(url_cerrar)
{
    if (url_cerrar != "javascript:void(0)")
    {
        $(".nav-cerrar-menu").removeClass("show");
    }
}

function class_redireccionar_url(url)
{
    if (url != "")
    {
        window.location = url;
    }
}

function class_emisora_ejercito()
{
    var x = document.getElementById("myAudio"); 
   
    if (playing_stream)
    {        
        playing_stream = false;
        $("#img-emisora").attr("src", "https://ejercito.mil.co/images/imag_video_enc_pause1.png");
        x.play();   
    }
    else
    { 
        playing_stream = true;
        $("#img-emisora").attr("src", "https://ejercito.mil.co/images/imag_video_enc_play1.png");
        x.pause();    
    }
}

function class_volver_arriba()
{
    $('html, body').animate({scrollTop:0}, 1250);
    return false;
}

function class_aumentar_letra()
{
    var sizeFuenteActual = $("html").css('font-size');
    var sizeFuenteActualNum = parseFloat(sizeFuenteActual, 10);
    var sizeFuenteNuevo = sizeFuenteActualNum*1.2;

    if(sizeFuenteNuevo < 25)
    {
        elemento.css('font-size', sizeFuenteNuevo);
    }
}

function class_disminuir_letra()
{
    var sizeFuenteActual = $("html").css('font-size');
    var sizeFuenteActualNum = parseFloat(sizeFuenteActual, 10);
    var sizeFuenteNuevo = sizeFuenteActualNum*0.8;

    if (sizeFuenteNuevo > 8)
    {
        elemento.css('font-size', sizeFuenteNuevo);
    }
}

function class_contraste()
{
    if (contrate == 0)
    {
        $("body").addClass("all");
        $(".gov-co-footer").css("background-image", "");
        $(".gov-co-footer-autoridad").css("background-color", "#000");
        $(".gov-co-footer-pie").css("background-color", "#000");
        $(".gov-co-footer .gov-co-footer-presetacion, .gov-co-footer-lg .gov-co-footer-presetacion, .gov-co-footer-sm .gov-co-footer-presetacion, .gov-co-footer-xl .gov-co-footer-presetacion, .gov-co-footer-xs .gov-co-footer-presetacion").css("background-color", "#000");
        $(".gov-co-footer .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-lg .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-sm .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-xl .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-xs .gov-co-footer-presetacion > .gov-co-footer-autoridad").css("background-color", "linear-gradient(to bottom,#000,#000 100%,#000 100%,#000 100%,#000 100%)");
        $(".menu_nabvar, .block-options, .interest-themes").css("background-color", "#000");
        $(".title, .card-title, .nav-link, .dropdown-item, .label-button-star-up, .title-govco-temas, .path-icono-rojo").css("color", "#fff");
        $(".btn").css("color", "#fff");
        $(".btn").css("border", "1px solid #fff");
        contrate = 1;

        if ($.cookie('contraste') != undefined)
        {
            var cache_javascript = "";
        }
        else
        {
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            text = "";
            
            for (var i = 0; i < 20; i++)
            {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }

            var cache_javascript = text;
            $.cookie('contraste', text, {expires: 365});
        }
    }
    else
    {
        $("body").removeClass("all");
        $(".gov-co-footer-autoridad").css("background-color", "");
        $(".gov-co-footer-pie").css("background-color", "#36c");
        $(".gov-co-footer .gov-co-footer-presetacion, .gov-co-footer-lg .gov-co-footer-presetacion, .gov-co-footer-sm .gov-co-footer-presetacion, .gov-co-footer-xl .gov-co-footer-presetacion, .gov-co-footer-xs .gov-co-footer-presetacion").css("background-color", "#fff");
        $(".gov-co-footer .gov-co-footer-presetacion, .gov-co-footer-lg .gov-co-footer-presetacion, .gov-co-footer-sm .gov-co-footer-presetacion, .gov-co-footer-xl .gov-co-footer-presetacion, .gov-co-footer-xs .gov-co-footer-presetacion").css("background-color", "#fff");
        $(".gov-co-footer .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-lg .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-sm .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-xl .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-xs .gov-co-footer-presetacion.gov-co-footer-tramites").css("background-color", "#fff");
        $(".gov-co-footer .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-lg .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-sm .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-xl .gov-co-footer-presetacion.gov-co-footer-tramites,.gov-co-footer-xs .gov-co-footer-presetacion.gov-co-footer-tramites").css("background-color", "#fff");
        $(".gov-co-footer .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-lg .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-sm .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-xl .gov-co-footer-presetacion > .gov-co-footer-autoridad, .gov-co-footer-xs .gov-co-footer-presetacion > .gov-co-footer-autoridad").css("background-color", "#fff");
        $(".menu_nabvar, .block-options, .interest-themes").css("background-color", "");
        $(".title, .nav-link, .dropdown-item, .label-button-star-up").css("color", "#323335");
        $(".nav-link.active").css("color", "#fff");
        $(".dropdown-item.active").css("color", "#fff");
        $(".news[_ngcontent-jvf-c8] .card-body[_ngcontent-jvf-c8] .card-title[_ngcontent-jvf-c8]").css("color", "#323335");
        $(".news[_ngcontent-jvf-c8] .card-body[_ngcontent-jvf-c8] .card-title[_ngcontent-jvf-c8]:hover").css("color", "#7c0808");
        $(".btn").css("color", "#323335");
        $(".btn").css("border", "1px solid #323335");
        $(".text-destacados").css("color", "#fff");
        $(".block-options").css("background-color", "#7c0808");
        $(".gov-co-footer-pie").css("background-color", "#7c0808");
        $(".title-govco-temas, .path-icono-rojo").css("color", "#7c0808");
        $(".collage-title").css("color", "#000");
        $(".title-podcast").css("color", "rgb(255, 255, 255)");
        $(".title-podcast-border").css("border", "1px solid rgb(255, 255, 255)");

        contrate = 0;

        if ($.cookie('contraste') != undefined)
        {
            $.removeCookie('contraste');
        }        
    }
}

function class_case_subsitios(variable_9, variable_10, elemento_array)
{
    switch (variable_9) 
    {
        case 'primera-division':
            array_home = array_home_principal[1];

            retorno = new Array();
            var ejecutar_ajax = $.ajax
            ({
                async: false, 	
                url: url_api,
                type: "POST",
                data: 
                {
                    funcion: 17,
                    nit: nit,
                    digito_verificacion: digito_verificacion,
                    array_home: array_home,
                    elemento_array, elemento_array
                },
                cache : false,			
                success: function(s)
                {	
                    retorno = JSON.parse(s);
                    
                    // Cabecera y pié de página
                    retorno_get = new Array();
                    var ejecutar_ajax = $.ajax
                    ({
                        async: false, 	
                        url: url_api_home_header_footer,
                        type: "GET",
                        cache: false,
                        datatype: "json",			
                        success: function(s)
                        {	
                            retorno_get = s; 

                            cabecera_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].cabecera_pagina_web;
                            pie_pagina_web           = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_web;    
                            nombre_empresa           = retorno_get["sys_parametrizacion_sistema"][0].nombre_empresa;
                            eslogan_empresa          = retorno_get["sys_parametrizacion_sistema"][0].eslogan_empresa;
                            direccion_empresa        = retorno_get["sys_parametrizacion_sistema"][0].direccion_empresa; 
                            telefono_empresa         = retorno_get["sys_parametrizacion_sistema"][0].telefono_empresa;
                            celular_empresa          = retorno_get["sys_parametrizacion_sistema"][0].celular_empresa;
                            correo_empresa           = retorno_get["sys_parametrizacion_sistema"][0].correo_empresa;
                            facebook_empresa         = retorno_get["sys_parametrizacion_sistema"][0].facebook_empresa;
                            instagram_empresa        = retorno_get["sys_parametrizacion_sistema"][0].instagram_empresa;
                            coordenadas_empresa_x    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_x;
                            coordenadas_empresa_y    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_y;
                            market                   = retorno_get["sys_parametrizacion_sistema"][0].market;
                            pie_pagina_empresa       = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_empresa;
                            quienes_somos            = retorno_get["sys_parametrizacion_sistema"][0].quienes_somos;        
                            url_imagen_empresa_while = retorno_get["sys_parametrizacion_sistema"][0].url_imagen_empresa_while;  
                            autor_pagina_web         = retorno_get["sys_parametrizacion_sistema"][0].autor_pagina_web;  
                            descripcion_pagina_web   = retorno_get["sys_parametrizacion_sistema"][0].descripcion_pagina_web;  
                            keywords_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].keywords_pagina_web;   
                            whatsapp_empresa         = retorno_get["sys_parametrizacion_sistema"][0].whatsapp_empresa;   
                            home_pagina_web          = retorno_get["sys_parametrizacion_sistema"][0].home_pagina_web;    
                            
                            $(".jcp_header").html(cabecera_pagina_web); 
                            $(".jcp_footer").html(pie_pagina_web); 
                            $(".jcp_direccion_empresa").html(direccion_empresa); 
                            $(".jcp_telefono_empresa").html(telefono_empresa); 
                            $(".jcp_correo_empresa").html(correo_empresa); 
                            $(".jcp_correo_empresa_href").attr("href", "mailto:"+correo_empresa); 

                            // Se modifica de acuerdo a subsitio o subpágina
                            $(".img-unidad").attr("src", "https://ejercito.mil.co/images/logo_escudo_ejc_div01.png");
                        },
                        error: function(jqXHR, textStatus, errorThrown){}
                    });        

                    $(".jcp_menu_principal").append(retorno[6]);
                    $(".jcp_banners").append(retorno[1]);
                    $(".jcp_home").html(home_pagina_web);             
                    $(".jcp_servicios").append(retorno[2]);
                    $(".jcp_marcadores").append(retorno[3]);
                    $(".jcp_multimedia_ejercito_tv").append(retorno[9]);  
                    $(".jcp_multimedia_podcast").append(retorno[8]);  
                    
                    setTimeout(function()
                    {
                        if ($.cookie('contraste') != undefined)
                        {
                            class_contraste();
                        }
                        class_slick_mas_videos();
                        class_podcast();
                    }, 100);    
                },
                error: function(jqXHR, textStatus, errorThrown){}
            });
            break;
        default:
            $(location).attr('href', elemento_array);
            break;
    }
}

function class_case_subsitios_imagen_logo(variable_11)
{
    switch (variable_11) 
    {
        case 'primera-division':
            $(".img-unidad").attr("src", "https://ejercito.mil.co/images/logo_escudo_ejc_div01.png");
            break;
    }
}

function class_slick_mas_videos()
{
    $('.slick-mas-videos').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
    });
}

function class_podcast()
{
    var pcastPlayers = document.querySelectorAll('.pcast-player');
    var speeds = [ 1, 1.5, 2, 2.5, 3 ]    
    
    for(i=0;i<pcastPlayers.length;i++) 
    {
        var player = pcastPlayers[i];
        var audio = player.querySelector('audio');
        var play = player.querySelector('.pcast-play');
        var pause = player.querySelector('.pcast-pause');
        var rewind = player.querySelector('.pcast-rewind');
        var progress = player.querySelector('.pcast-progress');
        var speed = player.querySelector('.pcast-speed');
        var mute = player.querySelector('.pcast-mute');
        var currentTime = player.querySelector('.pcast-currenttime');
        var duration = player.querySelector('.pcast-duration');
        
        var currentSpeedIdx = 0;

        pause.style.display = 'none';
        
        var toHHMMSS = function ( totalsecs ) {
            var sec_num = parseInt(totalsecs, 10); // don't forget the second param
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours; }
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}
            
            var time = hours+':'+minutes+':'+seconds;
            return time;
        }
        
        audio.addEventListener('loadedmetadata', function(){
        progress.setAttribute('max', Math.floor(audio.duration));
        duration.textContent  = toHHMMSS(audio.duration);
        });
        
        audio.addEventListener('timeupdate', function(){
        progress.setAttribute('value', audio.currentTime);
        currentTime.textContent  = toHHMMSS(audio.currentTime);
        });
        
        play.addEventListener('click', function(){
        this.style.display = 'none';
        pause.style.display = 'inline-block';
        pause.focus();
        audio.play();
        }, false);

        pause.addEventListener('click', function(){
        this.style.display = 'none';
        play.style.display = 'inline-block';
        play.focus();
        audio.pause();
        }, false);
    
        rewind.addEventListener('click', function(){
        audio.currentTime -= 30;
        }, false);
        
        progress.addEventListener('click', function(e){
        audio.currentTime = Math.floor(audio.duration) * (e.offsetX / e.target.offsetWidth);
        }, false);

        speed.addEventListener('click', function(){
        currentSpeedIdx = currentSpeedIdx + 1 < speeds.length ? currentSpeedIdx + 1 : 0;
        audio.playbackRate = speeds[currentSpeedIdx];
        this.textContent  = speeds[currentSpeedIdx] + 'x';
        return true;
        }, false);

        mute.addEventListener('click', function() {
        if(audio.muted) {
            audio.muted = false;
            this.querySelector('.bi').classList.remove('bi-volume-mute-fill');
            this.querySelector('.bi').classList.add('bi-volume-up-fill');
        } else {
            audio.muted = true;
            this.querySelector('.bi').classList.remove('bi-volume-up-fill');
            this.querySelector('.bi').classList.add('bi-volume-mute-fill');
        }
        }, false);
    }
}

var loc = window.location;
var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);

url_limpia =  loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
jcp_validar_boton_como_llegar = "";
url_limpia_padre = "https://"+window.location.hostname+"/";
url_limpia = url_limpia.replace('www.', '');

url_pagina.forEach(function(elemento_array, indice, array) 
{
    if (url_limpia_padre == elemento_array)
    {
        switch (elemento_array) 
        {
            case 'https://ejercito.mil.co/':
                if 
                (
                    url_limpia == elemento_array || 
                    url_limpia == elemento_array+"inicio/"
                )
                {
                    array_home = array_home_principal[0];

                    retorno = new Array();
                    var ejecutar_ajax = $.ajax
                    ({
                        async: false, 	
                        url: url_api,
                        type: "POST",
                        data: 
                        {
                            funcion: 17,
                            nit: nit,
                            digito_verificacion: digito_verificacion,
                            array_home: array_home,
                            elemento_array: elemento_array
                        },
                        cache : false,			
                        success: function(s)
                        {	
                            retorno = JSON.parse(s);
                            
                            // Cabecera y pié de página
                            retorno_get = new Array();
                            var ejecutar_ajax = $.ajax
                            ({
                                async: false, 	
                                url: url_api_home_header_footer,
                                type: "GET",
                                cache: false,
                                datatype: "json",			
                                success: function(s)
                                {	
                                    retorno_get = s; 

                                    cabecera_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].cabecera_pagina_web;
                                    pie_pagina_web           = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_web;    
                                    nombre_empresa           = retorno_get["sys_parametrizacion_sistema"][0].nombre_empresa;
                                    eslogan_empresa          = retorno_get["sys_parametrizacion_sistema"][0].eslogan_empresa;
                                    direccion_empresa        = retorno_get["sys_parametrizacion_sistema"][0].direccion_empresa; 
                                    telefono_empresa         = retorno_get["sys_parametrizacion_sistema"][0].telefono_empresa;
                                    celular_empresa          = retorno_get["sys_parametrizacion_sistema"][0].celular_empresa;
                                    correo_empresa           = retorno_get["sys_parametrizacion_sistema"][0].correo_empresa;
                                    facebook_empresa         = retorno_get["sys_parametrizacion_sistema"][0].facebook_empresa;
                                    instagram_empresa        = retorno_get["sys_parametrizacion_sistema"][0].instagram_empresa;
                                    coordenadas_empresa_x    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_x;
                                    coordenadas_empresa_y    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_y;
                                    market                   = retorno_get["sys_parametrizacion_sistema"][0].market;
                                    pie_pagina_empresa       = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_empresa;
                                    quienes_somos            = retorno_get["sys_parametrizacion_sistema"][0].quienes_somos;        
                                    url_imagen_empresa_while = retorno_get["sys_parametrizacion_sistema"][0].url_imagen_empresa_while;  
                                    autor_pagina_web         = retorno_get["sys_parametrizacion_sistema"][0].autor_pagina_web;  
                                    descripcion_pagina_web   = retorno_get["sys_parametrizacion_sistema"][0].descripcion_pagina_web;  
                                    keywords_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].keywords_pagina_web;   
                                    whatsapp_empresa         = retorno_get["sys_parametrizacion_sistema"][0].whatsapp_empresa;   
                                    home_pagina_web          = retorno_get["sys_parametrizacion_sistema"][0].home_pagina_web;    
                                    
                                    $(".jcp_header").html(cabecera_pagina_web); 
                                    $(".jcp_footer").html(pie_pagina_web); 
                                    $(".jcp_direccion_empresa").html(direccion_empresa); 
                                    $(".jcp_telefono_empresa").html(telefono_empresa); 
                                    $(".jcp_correo_empresa").html(correo_empresa); 
                                    $(".jcp_correo_empresa_href").attr("href", "mailto:"+correo_empresa); 
                                },
                                error: function(jqXHR, textStatus, errorThrown){}
                            });        

                            $(".jcp_menu_principal").append(retorno[6]);
                            $(".jcp_banners").append(retorno[1]);
                            $(".jcp_home").html(home_pagina_web);             
                            $(".jcp_servicios").append(retorno[2]);
                            $(".jcp_marcadores").append(retorno[3]);
                            $(".jcp_multimedia_ejercito_tv").append(retorno[9]);  
                            $(".jcp_multimedia_podcast").append(retorno[8]);                          
                            
                            setTimeout(function()
                            {   
                                if ($.cookie('contraste') != undefined)
                                {
                                    class_contraste();
                                }
                                class_slick_mas_videos();
                                class_podcast();
                            }, 100);   
                        },
                        error: function(jqXHR, textStatus, errorThrown){}
                    });
                }
                else
                { 
                    url_limpia = new URL(url_limpia).pathname;
                
                    array_home = 
                    [
                        [0, 0, url_limpia, 35]
                    ]

                    retorno = new Array();
                    var ejecutar_ajax = $.ajax
                    ({
                        async: false, 	
                        url: url_api,
                        type: "POST",
                        data: 
                        {
                            funcion: 17,
                            nit: nit,
                            digito_verificacion: digito_verificacion,
                            array_home: array_home,
                            elemento_array: elemento_array
                        },
                        cache : false,			
                        success: function(s)
                        {	
                            retorno = JSON.parse(s);

                            if (retorno[8] == 1)
                            {
                                class_case_subsitios(retorno[9], retorno[10], elemento_array);
                            }
                            else
                            {
                                if (retorno[0] == 3)
                                {               
                                    $(location).attr('href', elemento_array);
                                }
                                else
                                {        
                                    // Cabecera y pié de página
                                    retorno_get = new Array();
                                    var ejecutar_ajax = $.ajax
                                    ({
                                        async: false, 	
                                        url: url_api_home_header_footer,
                                        type: "GET",
                                        cache: false,
                                        datatype: "json",			
                                        success: function(s)
                                        {
                                            retorno_get = s; 

                                            cabecera_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].cabecera_pagina_web;
                                            pie_pagina_web           = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_web;    
                                            nombre_empresa           = retorno_get["sys_parametrizacion_sistema"][0].nombre_empresa;
                                            eslogan_empresa          = retorno_get["sys_parametrizacion_sistema"][0].eslogan_empresa;
                                            direccion_empresa        = retorno_get["sys_parametrizacion_sistema"][0].direccion_empresa; 
                                            telefono_empresa         = retorno_get["sys_parametrizacion_sistema"][0].telefono_empresa;
                                            celular_empresa          = retorno_get["sys_parametrizacion_sistema"][0].celular_empresa;
                                            correo_empresa           = retorno_get["sys_parametrizacion_sistema"][0].correo_empresa;
                                            facebook_empresa         = retorno_get["sys_parametrizacion_sistema"][0].facebook_empresa;
                                            instagram_empresa        = retorno_get["sys_parametrizacion_sistema"][0].instagram_empresa;
                                            coordenadas_empresa_x    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_x;
                                            coordenadas_empresa_y    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_y;
                                            market                   = retorno_get["sys_parametrizacion_sistema"][0].market;
                                            pie_pagina_empresa       = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_empresa;
                                            quienes_somos            = retorno_get["sys_parametrizacion_sistema"][0].quienes_somos;        
                                            url_imagen_empresa_while = retorno_get["sys_parametrizacion_sistema"][0].url_imagen_empresa_while;  
                                            autor_pagina_web         = retorno_get["sys_parametrizacion_sistema"][0].autor_pagina_web;  
                                            descripcion_pagina_web   = retorno_get["sys_parametrizacion_sistema"][0].descripcion_pagina_web;  
                                            keywords_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].keywords_pagina_web;   
                                            whatsapp_empresa         = retorno_get["sys_parametrizacion_sistema"][0].whatsapp_empresa;   
                                            home_pagina_web          = retorno_get["sys_parametrizacion_sistema"][0].home_pagina_web; 
                                            
                                            $(".jcp_header").html(cabecera_pagina_web); 
                                            $(".jcp_footer").html(pie_pagina_web); 
                                            $(".jcp_direccion_empresa").html(direccion_empresa); 
                                            $(".jcp_telefono_empresa").html(telefono_empresa); 
                                            $(".jcp_correo_empresa").html(correo_empresa); 
                                            $(".jcp_correo_empresa_href").attr("href", "mailto:"+correo_empresa); 
                                        },
                                        error: function(jqXHR, textStatus, errorThrown){}
                                    });   
                                        
                                    $(".jcp_menu_principal").append(retorno[2]);
                                    $(".jcp_banners").html(retorno[3]); 
                                    $(".jcp_home").html(retorno[1]); 
                                    $(".jcp_seccion_interna").html(retorno[4]); 

                                    class_case_subsitios_imagen_logo(retorno[11]);
                                }

                                setTimeout(function()
                                {
                                    $("."+retorno[6]).addClass("active");
                                    $("."+retorno[6]).css("color", "rgba(255, 255, 255, 0.9)");
                                                    
                                    url_limpia_final = url_limpia.replace('/','');
                                    url_limpia_final_2 = url_limpia_final.replace('/','');
                                    $("."+url_limpia_final_2).addClass("active");                
                                    $("."+url_limpia_final_2).css("color", "rgba(255, 255, 255, 0.9)");

                                    $("."+retorno[7]).addClass("active");                
                                    $("."+retorno[7]).css("color", "rgba(255, 255, 255, 0.9)");                

                                    if ($.cookie('contraste') != undefined)
                                    {                    
                                        class_contraste();                    
                                    }

                                    class_paginacion("", "");
                                }, 100);        
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){}
                    });
                }
                break;
            case 'https://primeradivision.mil.co/':
                if 
                (
                    url_limpia == elemento_array || 
                    url_limpia == elemento_array+"inicio/"
                )
                {
                    array_home = array_home_principal[1];

                    retorno = new Array();
                    var ejecutar_ajax = $.ajax
                    ({
                        async: false, 	
                        url: url_api,
                        type: "POST",
                        data: 
                        {
                            funcion: 17,
                            nit: nit,
                            digito_verificacion: digito_verificacion,
                            array_home: array_home,
                            elemento_array: elemento_array
                        },
                        cache : false,			
                        success: function(s)
                        {	
                            retorno = JSON.parse(s);
                            
                            // Cabecera y pié de página
                            retorno_get = new Array();
                            var ejecutar_ajax = $.ajax
                            ({
                                async: false, 	
                                url: url_api_home_header_footer,
                                type: "GET",
                                cache: false,
                                datatype: "json",			
                                success: function(s)
                                {	
                                    retorno_get = s; 

                                    cabecera_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].cabecera_pagina_web;
                                    pie_pagina_web           = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_web;    
                                    nombre_empresa           = retorno_get["sys_parametrizacion_sistema"][0].nombre_empresa;
                                    eslogan_empresa          = retorno_get["sys_parametrizacion_sistema"][0].eslogan_empresa;
                                    direccion_empresa        = retorno_get["sys_parametrizacion_sistema"][0].direccion_empresa; 
                                    telefono_empresa         = retorno_get["sys_parametrizacion_sistema"][0].telefono_empresa;
                                    celular_empresa          = retorno_get["sys_parametrizacion_sistema"][0].celular_empresa;
                                    correo_empresa           = retorno_get["sys_parametrizacion_sistema"][0].correo_empresa;
                                    facebook_empresa         = retorno_get["sys_parametrizacion_sistema"][0].facebook_empresa;
                                    instagram_empresa        = retorno_get["sys_parametrizacion_sistema"][0].instagram_empresa;
                                    coordenadas_empresa_x    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_x;
                                    coordenadas_empresa_y    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_y;
                                    market                   = retorno_get["sys_parametrizacion_sistema"][0].market;
                                    pie_pagina_empresa       = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_empresa;
                                    quienes_somos            = retorno_get["sys_parametrizacion_sistema"][0].quienes_somos;        
                                    url_imagen_empresa_while = retorno_get["sys_parametrizacion_sistema"][0].url_imagen_empresa_while;  
                                    autor_pagina_web         = retorno_get["sys_parametrizacion_sistema"][0].autor_pagina_web;  
                                    descripcion_pagina_web   = retorno_get["sys_parametrizacion_sistema"][0].descripcion_pagina_web;  
                                    keywords_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].keywords_pagina_web;   
                                    whatsapp_empresa         = retorno_get["sys_parametrizacion_sistema"][0].whatsapp_empresa;   
                                    home_pagina_web          = retorno_get["sys_parametrizacion_sistema"][0].home_pagina_web;    
                                    
                                    $(".jcp_header").html(cabecera_pagina_web); 
                                    $(".jcp_footer").html(pie_pagina_web); 
                                    $(".jcp_direccion_empresa").html(direccion_empresa); 
                                    $(".jcp_telefono_empresa").html(telefono_empresa); 
                                    $(".jcp_correo_empresa").html(correo_empresa); 
                                    $(".jcp_correo_empresa_href").attr("href", "mailto:"+correo_empresa); 
                                },
                                error: function(jqXHR, textStatus, errorThrown){}
                            });        

                            $(".jcp_menu_principal").append(retorno[6]);
                            $(".jcp_banners").append(retorno[1]);
                            $(".jcp_home").html(home_pagina_web);             
                            $(".jcp_servicios").append(retorno[2]);
                            $(".jcp_marcadores").append(retorno[3]);
                            $(".jcp_multimedia_ejercito_tv").append(retorno[9]);  
                            $(".jcp_multimedia_podcast").append(retorno[8]);  
                            
                            setTimeout(function()
                            {
                                if ($.cookie('contraste') != undefined)
                                {
                                    class_contraste();
                                }
                                class_slick_mas_videos();
                                class_podcast();
                                class_case_subsitios_imagen_logo("primera-division");
                            }, 100);   
                        },
                        error: function(jqXHR, textStatus, errorThrown){}
                    });
                }
                else
                { 
                    url_limpia = new URL(url_limpia).pathname;
                
                    array_home = 
                    [
                        [0, 0, url_limpia, 35]
                    ]

                    retorno = new Array();
                    var ejecutar_ajax = $.ajax
                    ({
                        async: false, 	
                        url: url_api,
                        type: "POST",
                        data: 
                        {
                            funcion: 17,
                            nit: nit,
                            digito_verificacion: digito_verificacion,
                            array_home: array_home,
                            elemento_array: elemento_array
                        },
                        cache : false,			
                        success: function(s)
                        {	
                            retorno = JSON.parse(s);

                            if (retorno[8] == 1)
                            {
                                class_case_subsitios(retorno[9], retorno[10], elemento_array);
                            }
                            else
                            {
                                if (retorno[0] == 3)
                                {                
                                    $(location).attr('href', elemento_array);
                                }
                                else
                                {        
                                    // Cabecera y pié de página
                                    retorno_get = new Array();
                                    var ejecutar_ajax = $.ajax
                                    ({
                                        async: false, 	
                                        url: url_api_home_header_footer,
                                        type: "GET",
                                        cache: false,
                                        datatype: "json",			
                                        success: function(s)
                                        {
                                            retorno_get = s; 

                                            cabecera_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].cabecera_pagina_web;
                                            pie_pagina_web           = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_web;    
                                            nombre_empresa           = retorno_get["sys_parametrizacion_sistema"][0].nombre_empresa;
                                            eslogan_empresa          = retorno_get["sys_parametrizacion_sistema"][0].eslogan_empresa;
                                            direccion_empresa        = retorno_get["sys_parametrizacion_sistema"][0].direccion_empresa; 
                                            telefono_empresa         = retorno_get["sys_parametrizacion_sistema"][0].telefono_empresa;
                                            celular_empresa          = retorno_get["sys_parametrizacion_sistema"][0].celular_empresa;
                                            correo_empresa           = retorno_get["sys_parametrizacion_sistema"][0].correo_empresa;
                                            facebook_empresa         = retorno_get["sys_parametrizacion_sistema"][0].facebook_empresa;
                                            instagram_empresa        = retorno_get["sys_parametrizacion_sistema"][0].instagram_empresa;
                                            coordenadas_empresa_x    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_x;
                                            coordenadas_empresa_y    = retorno_get["sys_parametrizacion_sistema"][0].coordenadas_empresa_y;
                                            market                   = retorno_get["sys_parametrizacion_sistema"][0].market;
                                            pie_pagina_empresa       = retorno_get["sys_parametrizacion_sistema"][0].pie_pagina_empresa;
                                            quienes_somos            = retorno_get["sys_parametrizacion_sistema"][0].quienes_somos;        
                                            url_imagen_empresa_while = retorno_get["sys_parametrizacion_sistema"][0].url_imagen_empresa_while;  
                                            autor_pagina_web         = retorno_get["sys_parametrizacion_sistema"][0].autor_pagina_web;  
                                            descripcion_pagina_web   = retorno_get["sys_parametrizacion_sistema"][0].descripcion_pagina_web;  
                                            keywords_pagina_web      = retorno_get["sys_parametrizacion_sistema"][0].keywords_pagina_web;   
                                            whatsapp_empresa         = retorno_get["sys_parametrizacion_sistema"][0].whatsapp_empresa;   
                                            home_pagina_web          = retorno_get["sys_parametrizacion_sistema"][0].home_pagina_web; 
                                            
                                            $(".jcp_header").html(cabecera_pagina_web); 
                                            $(".jcp_footer").html(pie_pagina_web); 
                                            $(".jcp_direccion_empresa").html(direccion_empresa); 
                                            $(".jcp_telefono_empresa").html(telefono_empresa); 
                                            $(".jcp_correo_empresa").html(correo_empresa); 
                                            $(".jcp_correo_empresa_href").attr("href", "mailto:"+correo_empresa); 
                                        },
                                        error: function(jqXHR, textStatus, errorThrown){}
                                    });   
                                        
                                    $(".jcp_menu_principal").append(retorno[2]);
                                    $(".jcp_banners").html(retorno[3]); 
                                    $(".jcp_home").html(retorno[1]); 
                                    $(".jcp_seccion_interna").html(retorno[4]); 

                                    class_case_subsitios_imagen_logo("primera-division");
                                }

                                setTimeout(function()
                                {
                                    $("."+retorno[6]).addClass("active");
                                    $("."+retorno[6]).css("color", "rgba(255, 255, 255, 0.9)");
                                                    
                                    url_limpia_final = url_limpia.replace('/','');
                                    url_limpia_final_2 = url_limpia_final.replace('/','');
                                    $("."+url_limpia_final_2).addClass("active");                
                                    $("."+url_limpia_final_2).css("color", "rgba(255, 255, 255, 0.9)");

                                    $("."+retorno[7]).addClass("active");                
                                    $("."+retorno[7]).css("color", "rgba(255, 255, 255, 0.9)");                

                                    if ($.cookie('contraste') != undefined)
                                    {                    
                                        class_contraste();                    
                                    }

                                    class_paginacion("", "");
                                }, 100);        
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){}
                    });
                }
                break;
        }
    }
})

